# Curves


## Project Structure

<table>
    <thead>
        <tr>
            <th colspan=5>← dependencies ←</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td colspan=4>insta.py</td>
            <td rowspan=3>poster.py</td>
        </tr>
        <tr>
            <td colspan=4>file_transfer.py</td>
        </tr>
        <tr>
            <td rowspan=2>deltas.py</td>
            <td>bezier.py</td>
            <td>canvas.py</td>
            <td>generator.py</td>
        </tr>
        <tr>
            <td colspan=4>metrics.py</td>
        </tr>
        <tr>
            <td colspan=5>ease.py</td>
        </tr>
    </tbody>
</table>


## Experiments to do

* Do the different bendiness metrics maintain the same order of magnitude
  for different `steps` values?
  
* Does the tolerance `tol` in `bezier_curve_equidistant` give stable results
  for different `steps` values?


## Ideas

* IDEA: plot distance between successive points (y) against parametrization value (x)
        and average over whole dataset. How does the distance evolve in the average bezier curve?
        If you can model this expected evolution, you could use it as a first correction to help
        bezier_curve_equidist converge faster.
  
* GradientCurveCanvas

