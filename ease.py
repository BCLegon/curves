from math import pi, cos


def c_ease(t):
    """
    Apply cosine easing to a time variable.
    0   ->   0
    1/2 -> 1/2
    1   ->   1
    3/4 -> 1/2
    2   ->   0
    @param t: float
    @return: float between 0 and 1.
    """
    return (1 - cos(t * pi)) / 2


def cc_ease(t):
    """
    Apply double cosine easing to a time variable.
    0   ->   0
    1/2 -> 1/2
    1   ->   1
    3/4 -> 1/2
    2   ->   0
    @param t: float
    @return: float between 0 and 1.
    """
    return c_ease(c_ease(t))
