from sanity import *
import numpy as np


assert is_number(0)
assert is_number(1)
assert is_number(-1)
assert is_number(0.23456)
assert is_number(float("inf"))
assert not is_number("inf")
assert not is_number("1")

assert is_iterable((0, 1, 2))
assert is_iterable(range(0, 2))
assert is_iterable([0, 1, 2])
assert is_iterable("hello world")
assert is_iterable(["a", "b", "c"])
assert is_iterable(dict(a=1, b=2))
assert is_iterable(np.zeros((2, 3)))
assert not is_iterable(123)

assert is_point(1)
assert is_point((0, 1, 2))
assert not is_point([(0, 1, 2)])
assert not is_point("123")
assert not is_point(np.zeros((2, 3)))

assert is_point_list([(0, 1, 2)])
assert is_point_list([(0, 1, 2), (1, 2, 3)])
assert is_point_list(np.zeros((2, 3)))

assert is_color((0, 0.5, 1))
assert is_color("#00AAFF")
assert is_color("#0AF")

assert is_stop(0)
assert is_stop(1)
assert is_stop(0.23456)
assert not is_stop(-1)
assert not is_stop(1.01)

assert is_curve([(1, 2), (3, 4), (5, 6)])
assert is_curve({"points": [[77.253, 12.509], [71.761, 56.913], [98.088, 18.811], [9.302, 80.099], [60.809, 82.646]]})
assert is_curve({"nb_points": 8, "points": [[77.253, 12.509], [71.761, 56.913], [98.088, 18.811], [9.302, 80.099], [98.581, 8.566], [93.686, 77.179], [12.147, 30.895], [60.809, 82.646]], "colors": ["#787AFF", "#787AFF"], "color_stops": [0, 1]})

assert is_composite_curve({"curves": [{"points": [[77.253, 12.509], [60.809, 82.646]]}, {"points": [[77.253, 12.509], [60.809, 82.646]]}]})
