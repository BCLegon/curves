from bezier import *
from elevation import elevate_bezier
import numpy as np


class BezierCalculation:
    """
    Class to cache curve calculations for efficient reuse.
    """

    def __init__(self, points, size=10):
        self.points = points
        self.size = size
        self.call_cache = []
        self.result_cache = []

    def _set_call_result(self, func, params, result):
        self.call_cache.append((func, *params))
        self.result_cache.append(result)
        while len(self.call_cache) > self.size:
            self.call_cache.pop(0)
            self.result_cache.pop(0)

    def _get_call_result(self, func, params):
        key = (func, *params)
        if key in self.call_cache:
            idx = self.call_cache.index(key)
            return self.result_cache[idx]
        else:
            return None

    def bezier_curve(self, steps=1000):
        result = self._get_call_result('bezier_curve', (steps,))
        if not result:
            result = bezier_curve(self.points, steps)
            self._set_call_result('bezier_curve', (steps,), result)
        return result

    def bezier_curve_equidistant(self, steps=1000, iters=20, tol=1e-10):
        result = self._get_call_result('bezier_curve_equidistant', (steps, iters, tol))
        if not result:
            result = bezier_curve_equidistant(self.points, steps, iters, tol)
            self._set_call_result('bezier_curve_equidistant', (steps, iters, tol), result)
        return result

    def bezier_curve_parametrized(self, t):
        result = self._get_call_result('bezier_curve_parametrized', (t,))
        if not result:
            result = bezier_curve_parametrized(self.points, t)
            self._set_call_result('bezier_curve_parametrized', (t,), result)
        return result


class BezierInterpolation:

    def __init__(self, points_a, points_b):
        self.points_a = points_a
        self.points_b = points_b
        self.calc_a = BezierCalculation(points_a)
        self.calc_b = BezierCalculation(points_b)

    def interpolate(self, alpha=0, steps=1000):
        return self.interpolate_control_points(alpha, steps)

    def interpolate_naive(self, alpha=0, steps=1000):
        axs, ays = self.calc_a.bezier_curve(steps)
        bxs, bys = self.calc_b.bezier_curve(steps)

        x_vals = (1 - alpha) * axs + alpha * bxs
        y_vals = (1 - alpha) * ays + alpha * bys

        return x_vals, y_vals

    def interpolate_equidistant(self, alpha=0, steps=1000, iters=20, tol=1e-10):
        axs, ays = self.calc_a.bezier_curve_equidistant(steps, iters, tol)
        bxs, bys = self.calc_b.bezier_curve_equidistant(steps, iters, tol)

        x_vals = (1 - alpha) * axs + alpha * bxs
        y_vals = (1 - alpha) * ays + alpha * bys

        return x_vals, y_vals

    def interpolate_control_points(self, alpha=0, steps=1000):
        self.match_order()
        points = (1 - alpha) * np.array(self.points_a) + alpha * np.array(self.points_b)

        x_vals, y_vals = bezier_curve(points, steps)

        return x_vals, y_vals

    def match_order(self):
        order_diff = len(self.points_a) - len(self.points_b)
        if order_diff < 0:
            self.points_a = elevate_bezier(self.points_a, -order_diff)
        elif order_diff > 0:
            self.points_b = elevate_bezier(self.points_b, order_diff)

        assert len(self.points_a) == len(self.points_b)

