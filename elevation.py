import numpy as np


def elevate_bezier(points, n=1):
    new_points = points
    for _ in range(n):
        new_points = _elevate_bezier(new_points)
    return new_points


def _stack_expand(points):
    first = np.pad(points, ((1, 0), (0, 0)), 'edge')
    second = np.pad(points, ((0, 1), (0, 0)), 'edge')
    return np.stack((first, second), axis=1)


def _elevation_weights(points):
    new_order = len(points)
    first = np.arange(new_order + 1) / new_order
    second = np.flip(first)
    return np.stack((first, second), axis=1)


def _elevate_bezier(points):
    """
    new_pt = ((n - i) * pt_i + i * pt_j) / n
    with j = i - 1
    """
    stacked_points = _stack_expand(points)
    weights = _elevation_weights(points)
    weighted_points = np.multiply(stacked_points, weights[:, :, np.newaxis])
    return np.sum(weighted_points, axis=1)
