import numbers
from matplotlib.colors import is_color_like


def is_number(item):
    return isinstance(item, numbers.Number)


def is_iterable(item):
    try:
        for i in item:
            return True
    except (ValueError, TypeError):
        return False


def is_point(item):
    if is_number(item):
        return True
    elif is_iterable(item):
        for i in item:
            return is_number(i)
    else:
        return False


def is_point_list(item):
    if is_iterable(item):
        for i in item:
            return is_point(i)
    else:
        return False


def is_color(item):
    return is_color_like(item)


def is_color_list(item):
    if is_iterable(item):
        for i in item:
            return is_color(i)
    else:
        return False


def is_stop(item):
    if is_number(item):
        if 0 <= item <= 1:
            return True
    return False


def is_stop_list(item):
    if is_iterable(item):
        for i in item:
            return is_stop(i)
    else:
        return False


def is_wrapped_curve(item):
    if not isinstance(item, dict):
        return False
    try:
        return is_point_list(item["points"])
    except KeyError:
        return False


def is_curve(item):
    if is_point_list(item):
        return True
    elif is_wrapped_curve(item):
        return True


def is_curve_list(item):
    if is_iterable(item):
        for i in item:
            return is_curve(i)
    else:
        return False


def is_wrapped_curve_list(item):
    if not isinstance(item, dict):
        return False
    try:
        return is_curve_list(item["curves"])
    except KeyError:
        return False


def is_composite_curve(item):
    if is_curve_list(item):
        return True
    elif is_wrapped_curve_list(item):
        return True
