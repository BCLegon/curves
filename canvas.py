from bezier import bezier_curve
import matplotlib.pyplot as plt
import matplotlib.colors as matcol
import colorsys
import numpy as np
import io
from PIL import Image


class CurveCanvas:
    """
    A class for rendering Bézier curves.
    """

    def __init__(self, points, line_color='#000000', background_color='#FFFFFF', thickness=10, margin=0.2):
        """
        Create a new canvas with the given parameters.
        """
        if len(points) < 2:
            raise ValueError('Number of points should be 2 or more')
        self.points = points

        # Interesting to change
        self.line_color = line_color
        self.background_color = background_color
        self.thickness = thickness
        self.margin = margin

        # Not necessary to change
        self.steps = 1000
        self.ending = 'round'  # 'butt', 'round' or 'projecting'
        self.corners = 'round'  # 'miter', 'round' or 'bevel'
        self.dpi = 300
        self.figsize = (6, 6)  # Actual figsize differs since pyplot borders are removed

    def render(self):
        """
        Renders the curve and returns a PIL Image object in PNG format.
        """
        # Calculations
        xs, ys = bezier_curve(self.points, self.steps)
        x_lim, y_lim = self.get_box(xs, ys, margin=self.margin)

        # Visualization
        plt.figure(figsize=self.figsize, dpi=self.dpi)
        plt.plot(xs, ys, color=self.line_color, linewidth=self.thickness,
                 solid_capstyle=self.ending, solid_joinstyle=self.corners)
        plt.rcParams['savefig.facecolor'] = self.background_color

        # Set axes
        plt.xlim(*x_lim)
        plt.ylim(*y_lim)
        plt.gca().invert_yaxis()
        plt.gca().set_aspect(1.0, adjustable='box')
        plt.axis('off')

        # Save image to buffer
        buffer = io.BytesIO()
        plt.savefig(buffer, format='png', bbox_inches='tight', pad_inches=0)
        buffer.seek(0)
        return Image.open(buffer)

    def save(self, file):
        """
        Renders the canvas and saves the result to a file path or file object.
        Defaults to PNG format if the format cannot be determined from the file name.
        """
        image = self.render()
        try:
            image.save(file)
        except ValueError:
            image.save(file, format='png')

    @staticmethod
    def get_box(xs, ys, margin=0.0):
        """
        Calculates limits for a centered square box around the given data points.
        """
        factor = 1.0 + 2 * margin
        min_x, max_x = np.min(xs), np.max(xs)
        min_y, max_y = np.min(ys), np.max(ys)

        range_x = max_x - min_x
        range_y = max_y - min_y
        range_ = np.max((factor * range_x, factor * range_y))

        min_x -= (range_ - range_x) / 2
        max_x += (range_ - range_x) / 2
        min_y -= (range_ - range_y) / 2
        max_y += (range_ - range_y) / 2
        return (min_x, max_x), (min_y, max_y)


class ColorInterpolation:

    @staticmethod
    def _get_hls_tints_shades(hex_color, steps=5, limit=0.0):
        rgb_color = matcol.to_rgb(hex_color)
        hls_color = colorsys.rgb_to_hls(*rgb_color)

        l_shades = np.linspace(limit, hls_color[1], steps)
        h_shades = hls_color[0] * np.ones_like(l_shades)
        s_shades = hls_color[2] * np.ones_like(l_shades)

        hls_shades = np.column_stack([h_shades, l_shades, s_shades])
        hex_shades = [matcol.to_hex(colorsys.hls_to_rgb(*shade)) for shade in hls_shades]
        return hex_shades

    @staticmethod
    def _get_hsv_tints_shades(hex_color, steps=5, limit=0.0):
        pass

    @staticmethod
    def _get_rgb_tints_shades(hex_color, steps=5, limit=0.0):
        rgb_color = matcol.to_rgb(hex_color)
        if isinstance(limit, (int, float)):
            limit = (limit, limit, limit)
        elif isinstance(limit, str):
            limit = matcol.to_rgb(limit)

        r_shades = np.linspace(limit[0], rgb_color[0], steps)
        g_shades = np.linspace(limit[1], rgb_color[1], steps)
        b_shades = np.linspace(limit[2], rgb_color[2], steps)

        rgb_shades = np.column_stack([r_shades, g_shades, b_shades])
        hex_shades = [matcol.to_hex(shade) for shade in rgb_shades]
        return hex_shades

    @staticmethod
    def get_shades(hex_color, steps=5):
        return ColorInterpolation._get_rgb_tints_shades(hex_color, steps, 0.0)

    @staticmethod
    def get_tints(hex_color, steps=5):
        return ColorInterpolation._get_rgb_tints_shades(hex_color, steps, 1.0)

    @staticmethod
    def get_tones(hex_color, steps=5):
        return ColorInterpolation._get_rgb_tints_shades(hex_color, steps, 0.5)


class ReplicatedCurveCanvas(CurveCanvas):
    """
    A class for rendering replicated Bézier curves.
    """

    def __init__(self, points, line_color_front='#000000', line_color_back='#999999', line_color_steps=5,
                 background_color='#FFFFFF', thickness=10, margin=0.2):
        super(ReplicatedCurveCanvas, self).__init__(points, line_color_front, background_color, thickness, margin)
        self.line_color_front = line_color_front
        self.line_color_back = line_color_back
        self.line_color_steps = line_color_steps

    @property
    def line_colors(self):
        return ColorInterpolation._get_rgb_tints_shades(self.line_color_front, self.line_color_steps, self.line_color_back)

    def render(self):
        """
        Renders the curve and returns a PIL Image object in PNG format.
        """

        # Calculations
        xs, ys = bezier_curve(self.points, self.steps)
        offset = 1
        #print(xs[offset:] - xs[:-offset])
        #print(ys[offset:] - ys[:-offset])
        x_diffs = (xs[offset:] - xs[:-offset])
        y_diffs = (ys[offset:] - ys[:-offset])
        diff_dir = y_diffs / x_diffs
        diff_norm = np.linalg.norm([x_diffs, y_diffs], axis=0)
        avg_tan = np.average(diff_dir, weights=diff_norm) # avg or median? No, better: weighed average with vector norms

        angle = np.arctan(avg_tan) + np.pi / 2

        x_lim, y_lim = self.get_box(xs, ys, margin=self.margin)
        size = x_lim[1] - x_lim[0]
        increment_x = size / 50 * np.cos(angle)
        increment_y = size / 50 * np.sin(angle)

        # Visualization
        all_xs = []
        all_ys = []
        plt.figure(figsize=self.figsize, dpi=self.dpi)
        for color in self.line_colors:
            all_xs.append(xs)
            all_ys.append(ys)
            plt.plot(xs, ys, color=color, linewidth=self.thickness,
                     solid_capstyle=self.ending, solid_joinstyle=self.corners)
            xs = xs - increment_x
            ys = ys - increment_y
        plt.rcParams['savefig.facecolor'] = self.background_color

        # just recompute get_box here with all xs and ys
        xs = np.array(all_xs).flatten()
        ys = np.array(all_ys).flatten()
        x_lim, y_lim = self.get_box(xs, ys, margin=self.margin)

        # Set axes
        plt.xlim(*x_lim)
        plt.ylim(*y_lim)
        plt.gca().invert_yaxis()
        plt.gca().set_aspect(1.0, adjustable='box')
        plt.axis('off')

        # Save image to buffer
        buffer = io.BytesIO()
        plt.savefig(buffer, format='png', bbox_inches='tight', pad_inches=0)
        buffer.seek(0)
        return Image.open(buffer)


class ImpliedCurveCanvas(ReplicatedCurveCanvas):
    """
    A class for rendering replicated Bézier curves, hiding the front curve.
    """

    @property
    def line_colors(self):
        colors = ColorInterpolation._get_rgb_tints_shades(self.line_color_front, self.line_color_steps, self.line_color_back)
        return colors + [self.background_color]


class PrideCurveCanvas(ReplicatedCurveCanvas):
    """
    A class for rendering replicated Bézier curves with a five-color rainbow palette.
    """

    @property
    def line_colors(self):
        return ['#ac92eb', '#4fc1e8', '#a0d568', '#ffce54', '#ed5564']


class GradientCurveCanvas(CurveCanvas):

    def __init__(self, points, colors=('#000000',), color_pos=(0,), background_color='#FFFFFF', thickness=10, margin=0.2):
        raise NotImplementedError

