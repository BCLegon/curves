# Based on https://stackoverflow.com/questions/12643079/bézier-curve-fitting-with-scipy
import numpy as np
from scipy.special import comb
from deltas import euclidean_deltas


def bernstein_poly(i, n, t):
    """
    The Bernstein polynomial of n, i as a function of t
    """
    return comb(n, i) * (t**(n - i)) * (1 - t)**i


def bezier_curve(cp, steps=1000):
    """
    Given a set of control points, return the
    bezier curve defined by the control points.

    cp should be a list of lists, or list of tuples
    such as [ [1,1],
              [2,3],
              [4,5],
              ...
              [Xn, Yn] ]
    steps is the number of time steps, defaults to 1000

    Returns a tuple (x_vals, y_vals) containing two numpy arrays
    of respectively x and y coordinates.

    See http://processingjs.nihongoresources.com/bezierinfo/
    """
    nb_cp = len(cp)
    x_cp = np.array([p[0] for p in cp])
    y_cp = np.array([p[1] for p in cp])
    t = np.linspace(0.0, 1.0, steps)

    polynomials = np.array([bernstein_poly(i, nb_cp - 1, t) for i in range(nb_cp)])

    x_vals = np.dot(x_cp, polynomials)
    y_vals = np.dot(y_cp, polynomials)
    return x_vals, y_vals


def bezier_curve_equidistant(cp, steps=1000, iters=20, tol=1e-10):
    """
    Same as `bezier_curve(cp, steps)`, but spreads interpolation points evenly along the curve.
    Runs until convergence within `tol` or until it reaches `iters` iterations, whichever comes first.
    """
    # First timestep and curve calculation
    t = np.linspace(0.0, 1.0, steps)
    x_vals, y_vals = bezier_curve_parametrized(cp, t)

    for _ in range(iters):
        # Reshaping for easier use
        t = np.vstack(t)
        vals = np.column_stack([x_vals, y_vals])
        # Get consecutive distances and time differences
        t_deltas = euclidean_deltas(t)
        d_deltas = euclidean_deltas(vals)
        # Convergence check using relative variance
        var = np.var(d_deltas / np.sum(d_deltas))
        if var < tol:
            break
        # Compute new timesteps using deltas
        new_t_deltas = (t_deltas / d_deltas)
        new_t_deltas = new_t_deltas / np.sum(new_t_deltas)
        t = np.concatenate(([0], np.cumsum(new_t_deltas)), axis=0)
        # New curve calculation
        x_vals, y_vals = bezier_curve_parametrized(cp, t)

    return x_vals, y_vals


def bezier_curve_parametrized(cp, t):
    """
    Same as `bezier_curve(cp, steps)`, but takes an array of parametrization values `t`
    instead of a number of steps.
    """
    nb_cp = len(cp)
    x_cp = np.array([p[0] for p in cp])
    y_cp = np.array([p[1] for p in cp])

    polynomials = np.array([bernstein_poly(i, nb_cp - 1, t) for i in range(nb_cp)])

    x_vals = np.dot(x_cp, polynomials)
    y_vals = np.dot(y_cp, polynomials)
    return x_vals, y_vals
