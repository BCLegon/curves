import numpy as np
import numpy.linalg as la


def euclidean_deltas(points):
    """
    Compute the euclidean distances between consecutive points
    @param points: array of shape (n, d) with n number of points, and d number of dimensions
    @return: array of shape (n-1,)
    """
    # From https://stackoverflow.com/a/13592234
    deltas = np.diff(points, axis=0)
    return np.sqrt((deltas ** 2).sum(axis=1))


def area_deltas(points):
    """
    Compute the areas span by triplets of consecutive points.
    Uses the cross product of the vectors between these points, which may be negative.
    @param points: array of shape (n, d) with n number of points, and d number of dimensions
    @return: array of shape (n-2,)
    """
    # From https://stackoverflow.com/a/13592234
    deltas = np.diff(points, axis=0)

    n = deltas.shape[0] - 1
    areas = np.empty(n)
    for i in range(n):
        # Take the opposite, to correct for vector orientation
        areas[i] = - np.cross(deltas[i], deltas[i+1])

    return areas


def angle_deltas(points):
    """
    Compute the sines of the angles span by triplets of consecutive points.
    Uses the cross product of the vectors between these points, which may be negative.
    @param points: array of shape (n, d) with n number of points, and d number of dimensions
    @return: array of shape (n-2,)
    """
    # From https://stackoverflow.com/a/13592234
    deltas = np.diff(points, axis=0)

    n = deltas.shape[0] - 1
    angles = np.empty(n)
    for i in range(n):
        # Take the opposite, to correct for vector orientation
        angles[i] = - np.cross(deltas[i], deltas[i+1]) / (la.norm(deltas[i]) * la.norm(deltas[i+1]))

    return angles


def projection_deltas(points):
    """
    Compute projections from triplets of consecutive points.
    Uses the cross product of the vectors between these points, which may be negative.
    Each vector b is projected onto the perpendicular on the previous vector a, resulting in |b| * sin(a^b).
    @param points: array of shape (n, d) with n number of points, and d number of dimensions
    @return: array of shape (n-2,)
    """
    # From https://stackoverflow.com/a/13592234
    deltas = np.diff(points, axis=0)

    n = deltas.shape[0] - 1
    projections = np.empty(n)
    for i in range(n):
        # Take the opposite, to correct for vector orientation
        projections[i] = - np.cross(deltas[i], deltas[i+1]) / (la.norm(deltas[i]))

    return projections
