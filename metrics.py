from deltas import *
from bezier import bezier_curve


def density_bendiness(points, steps=1000):
    """
    Returns a number that allows for relative bendiness comparison between Bézier curves.
    No guarantees are given about the actual values. These may depend on the number of interpolation steps.

    Rationale: equidistant parameter values are spread evenly across control points,
    not across euclidean distance traveled over the resulting curve.
    When there is a mismatch between those two distance, it implies that control points
    lie close together, often resulting in sharp turns and twists.
    This leads to the following property: Bézier curves have a higher density of interpolation values
        in places where the curve bends more heavily.
    The opposite is not guaranteed: a higher density of interpolation points does not imply heavy bends.
    This method assumes the opposite.
    """
    # Curve calculation
    x_vals, y_vals = bezier_curve(points, steps)
    # Reshaping for easier use
    vals = np.column_stack([x_vals, y_vals])
    # Get consecutive distances
    d_deltas = euclidean_deltas(vals)
    # Convergence check using relative variance
    var = np.var(d_deltas / np.sum(d_deltas))
    return var * steps * steps


def bendiness(points, steps=1000, method=projection_deltas):
    """
    Returns a number that allows for relative bendiness comparison between Bézier curves.
    No guarantees are given about the actual values. These may depend on the number of interpolation steps.
    """
    # Curve calculation
    x_vals, y_vals = bezier_curve(points, steps)
    # Reshaping for easier use
    vals = np.column_stack([x_vals, y_vals])
    # Get consecutive distances
    d_deltas = np.abs(method(vals))

    return np.sum(d_deltas)


def projection_bendiness(points, steps=1000):
    return bendiness(points, steps, method=projection_deltas)


def angle_bendiness(points, steps=1000):
    return bendiness(points, steps, method=angle_deltas)


def area_bendiness(points, steps=1000):
    return bendiness(points, steps, method=area_deltas)
