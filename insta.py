import json
import requests


class APIError(Exception):
    """
    For when the API returns an error.
    """

    def __init__(self, response):
        self.response = response

        if 'message' in response:
            message = response['message']
        else:
            message = response
        super().__init__(message)


class IGGateway:
    """
    Responsibilities:
    * interacting with API itself
    * generating well-formed urls
    * ensuring the correct request type
    * preparing request payload
    * noticing API errors and raising them as Python exceptions
    """
    base_url = 'https://graph.facebook.com/v11.0/{graph_id}'

    def __init__(self, access_token):
        self.access_token = access_token

    def _prepare_url(self, graph_id, edge=None):
        if edge:
            return self.base_url.format(graph_id=graph_id) + '/' + edge
        else:
            return self.base_url.format(graph_id=graph_id)

    def _prepare_fields_payload(self, fields=None, fields_name='fields'):
        if fields:
            fields_string = ','.join(fields)
            payload = {fields_name: fields_string, 'access_token': self.access_token}
        else:
            payload = {'access_token': self.access_token}
        return payload

    def _prepare_multi_payload(self, payload=None):
        if payload:
            payload['access_token'] = self.access_token
        else:
            payload = {'access_token': self.access_token}
        return payload

    def get_accounts(self, user_id='me'):
        url = self._prepare_url(user_id, 'accounts')
        payload = self._prepare_fields_payload()
        return self.check_error(requests.get(url, params=payload).json())

    def get_ig_business(self, page_id):
        url = self._prepare_url(page_id)
        payload = self._prepare_fields_payload(['instagram_business_account'])
        return self.check_error(requests.get(url, params=payload).json())

    def get_media(self, user_id):
        url = self._prepare_url(user_id, 'media')
        payload = self._prepare_fields_payload()
        return self.check_error(requests.get(url, params=payload).json())

    def get_media_details(self, media_id):
        url = self._prepare_url(media_id)
        payload = self._prepare_fields_payload(['id', 'ig_id', 'media_type', 'media_url', 'username', 'timestamp', 'caption', 'comments_count', 'like_count', 'shortcode'])
        return self.check_error(requests.get(url, params=payload).json())

    def get_insights(self, media_id):
        url = self._prepare_url(media_id, 'insights')
        payload = self._prepare_fields_payload(['engagement', 'impressions', 'reach', 'saved'], 'metric')
        return self.check_error(requests.get(url, params=payload).json())

    def prepare_media(self, user_id, image_url, caption=''):
        """
        Photo Requirements
            Maximum file size: 8MiB
            Aspect ratio: Must be within a 4:5 to 1.91:1 range
            Minimum width: 320 (will be scaled up to the minimum if necessary)
            Maximum width: 1440 (will be scaled down to the maximum if necessary)
            Height: Varies, depending on width and aspect ratio
            Formats: JPEG
        """
        url = self._prepare_url(user_id, 'media')
        payload = self._prepare_multi_payload({'image_url': image_url, 'caption': caption})
        return self.check_error(requests.post(url, params=payload).json())

    def publish_media(self, user_id, creation_id):
        url = self._prepare_url(user_id, 'media_publish')
        payload = self._prepare_multi_payload({'creation_id': creation_id})
        return self.check_error(requests.post(url, params=payload).json())

    @staticmethod
    def check_error(response):
        if 'error' in response:
            raise APIError(response['error'])
        else:
            return response

    def debug_token(self, token):
        url = self._prepare_url('debug_token')
        payload = self._prepare_multi_payload({'input_token': token})
        return self.check_error(requests.get(url, params=payload).json())


class IGNavigationError(Exception):
    """
    For when a response does not contain the expected keys.
    """

    def __init__(self, response, key, message=None):
        self.message = '{error}\n    key: {key}\n    response: {response}'.format(
            error=message,
            key=key,
            response=response
        )
        super().__init__(self.message)
